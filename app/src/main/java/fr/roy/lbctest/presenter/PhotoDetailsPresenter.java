package fr.roy.lbctest.presenter;

import android.net.Uri;

import java.lang.ref.WeakReference;

import fr.roy.lbctest.contract.PhotoDetailsContract;
import fr.roy.lbctest.model.Photo;

/**
 * Presenter used for displaying a photo.
 */
public class PhotoDetailsPresenter implements PhotoDetailsContract.Presenter {

    private Photo mPhoto;

    private final WeakReference<PhotoDetailsContract.View> mViewWeakReference;

    public PhotoDetailsPresenter(PhotoDetailsContract.View view) {
        mViewWeakReference = new WeakReference<>(view);
    }

    @Override
    public void setPhoto(Photo photo) {
        mPhoto = photo;
    }

    @Override
    public void start() {
        PhotoDetailsContract.View view = mViewWeakReference.get();
        if (view == null) {
            return;
        }
        view.displayPhotoName(mPhoto.getTitle());
        view.setPhotoUri(Uri.parse(mPhoto.getUrl()));

    }

    @Override
    public Photo getPhoto() {
        return mPhoto;
    }
}
