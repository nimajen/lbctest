package fr.roy.lbctest.presenter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import fr.roy.lbctest.contract.PhotoListContract;
import fr.roy.lbctest.model.Photo;
import fr.roy.lbctest.model.PhotoRetriever;
import fr.roy.lbctest.model.source.PhotoRetrieverImpl;

/**
 * Presenter for the photo list.
 */
public class PhotoListPresenter implements PhotoListContract.Presenter {

    private final WeakReference<PhotoListContract.View> mPhotoListView;

    public PhotoListPresenter(PhotoListContract.View view) {
        mPhotoListView = new WeakReference<>(view);
    }

    @Override
    public void start() {
        retrievePhoto(false);
    }

    @Override
    public void onRefreshClick() {
        PhotoListContract.View photoListView = mPhotoListView.get();
        if (photoListView == null) {
            return;
        }
        // display empty list
        photoListView.displayPhotoList(new ArrayList<Photo>());
        // and request for the new photo list
        retrievePhoto(true);
    }

    private void retrievePhoto(boolean refreshData) {
        PhotoListContract.View photoListView = mPhotoListView.get();
        if (photoListView == null) {
            return;
        }
        photoListView.showLoader();
        PhotoRetriever photoRetriever = PhotoRetrieverImpl.getInstance();
        photoRetriever.loadPhotos(new PhotoRetriever.LoadPhotoCallback() {
            @Override
            public void onPhotoLoaded(List<Photo> photoList) {
                PhotoListContract.View photoListView = mPhotoListView.get();
                if (photoListView == null) {
                    return;
                }
                photoListView.stopLoader();
                photoListView.displayPhotoList(photoList);
            }

            @Override
            public void onLoadError() {
                PhotoListContract.View photoListView = mPhotoListView.get();
                if (photoListView == null) {
                    return;
                }
                photoListView.stopLoader();
                photoListView.displayError();
            }
        }, refreshData);
    }

}
