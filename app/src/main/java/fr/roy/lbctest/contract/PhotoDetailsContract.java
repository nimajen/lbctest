package fr.roy.lbctest.contract;

import android.net.Uri;

import fr.roy.lbctest.model.Photo;

/**
 * Contracts for the photo list.
 */
public interface PhotoDetailsContract {

    /**
     * Contract for the view. Contains the method called by the presenter.
     */
    interface View {

        /**
         * Display the photo name
         *
         * @param name the name of the photo
         */
        void displayPhotoName(String name);

        /**
         * Set the photo uri to fresco
         *
         * @param uri the uri to use
         */
        void setPhotoUri(Uri uri);

    }

    /**
     * Contract for the presenter. Contains the method called by the view.
     */
    interface Presenter {

        /**
         * Set the photo to the presenter
         *
         * @param photo the photo to use
         */
        void setPhoto(Photo photo);

        /**
         * Called when the presenter must start.
         */
        void start();

        /**
         * Returns the photo linked with the presenter
         *
         * @return the linked photo
         */
        Photo getPhoto();

    }
}
