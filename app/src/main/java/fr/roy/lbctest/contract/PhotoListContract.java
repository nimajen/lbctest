package fr.roy.lbctest.contract;

import java.util.List;

import fr.roy.lbctest.model.Photo;

/**
 * Contracts for the photo list.
 */
public interface PhotoListContract {

    /**
     * Contract for the view. Contains the method called by the presenter.
     */
    interface View {

        /**
         * Display a loader
         */
        void showLoader();

        /**
         * Hide the loader
         */
        void stopLoader();

        /**
         * Display a message of error.
         */
        void displayError();

        /**
         * Display the photo list
         *
         * @param photoList the photo list to display
         */
        void displayPhotoList(List<Photo> photoList);
    }

    /**
     * Contract for the presenter. Contains the method called by the view.
     */
    interface Presenter {

        /**
         * Called when the presenter must start the photo request
         */
        void start();

        /**
         * Called when the user click on the refresh button
         */
        void onRefreshClick();
    }
}
