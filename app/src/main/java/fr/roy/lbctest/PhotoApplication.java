package fr.roy.lbctest;

import com.facebook.drawee.backends.pipeline.Fresco;

import android.app.Application;

import io.realm.Realm;

/**
 * Application class for LCBTest. Contains specific initialization.
 */
public class PhotoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        Realm.init(this);
    }
}
