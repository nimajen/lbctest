package fr.roy.lbctest.adapter;

import com.facebook.drawee.view.SimpleDraweeView;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.roy.lbctest.R;
import fr.roy.lbctest.model.Photo;

/**
 * Display photo element in a recycler view
 */
public class PhotoListAdapter extends RecyclerView.Adapter {

    private List<Photo> mPhotoList = new ArrayList<>();

    private final PhotoListAdapterListener mListener;

    public PhotoListAdapter(PhotoListAdapterListener listAdapterListener) {
        mListener = listAdapterListener;
    }

    public void setPhotoList(List<Photo> photoList) {
        mPhotoList = photoList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_photo, parent, false);
        return new PhotoItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final PhotoItemHolder holder = (PhotoItemHolder) viewHolder;
        final Photo photo = mPhotoList.get(position);
        holder.mPhotoName.setText(photo.getTitle());
        Uri uri = Uri.parse(photo.getThumbnailUrl());
        holder.mThumbnail.setImageURI(uri);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onPhotoClicked(photo);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPhotoList.size();
    }

    static class PhotoItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cell_photo_name)
        TextView mPhotoName;

        @BindView(R.id.cell_photo_thumbnail)
        SimpleDraweeView mThumbnail;

        PhotoItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface PhotoListAdapterListener {

        void onPhotoClicked(Photo photo);
    }

}
