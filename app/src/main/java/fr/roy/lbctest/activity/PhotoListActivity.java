package fr.roy.lbctest.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import fr.roy.lbctest.R;
import fr.roy.lbctest.adapter.PhotoListAdapter;
import fr.roy.lbctest.contract.PhotoListContract;
import fr.roy.lbctest.model.Photo;
import fr.roy.lbctest.presenter.PhotoListPresenter;

/**
 * Display the photo from the webservice getPhotos into a list.
 */
public class PhotoListActivity extends AppCompatActivity implements PhotoListContract.View, PhotoListAdapter.PhotoListAdapterListener {

    private static final String SHOW_PROGRESS_KEY = "SHOW_PROGRESS";

    private PhotoListAdapter mPhotoListAdapter;

    private ProgressDialog mProgressDialog;

    /**
     * True if the progress dialog is showing. We need to stop the progress dialog in onPause and restart it in onResume.
     */
    private boolean mShowProgressDialog;

    private PhotoListContract.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_list);
        initProgressBar();
        initRecyclerView();
        initToolbar();
        initPhotoListPresenter();
        if (savedInstanceState != null) {
            mShowProgressDialog = savedInstanceState.getBoolean(SHOW_PROGRESS_KEY, false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mShowProgressDialog && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(SHOW_PROGRESS_KEY, mShowProgressDialog);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.photo_list_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                mPresenter.onRefreshClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void showLoader() {
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
        mShowProgressDialog = true;
    }

    @Override
    public void stopLoader() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mShowProgressDialog = false;
    }

    @Override
    public void displayError() {
        Toast.makeText(this, getString(R.string.error_message_ws), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayPhotoList(List<Photo> photoList) {
        mPhotoListAdapter.setPhotoList(photoList);
    }

    @Override
    public void onPhotoClicked(Photo photo) {
        Intent intent = PhotoDetailsActivity.getIntent(this, photo);
        startActivity(intent);
    }

    private void initPhotoListPresenter() {
        mPresenter = new PhotoListPresenter(this);
        mPresenter.start();
    }

    private void initProgressBar() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.loading_in_progress));
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.activity_photo_list_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mPhotoListAdapter = new PhotoListAdapter(this);
        recyclerView.setAdapter(mPhotoListAdapter);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_photo_list_toolbar);
        setSupportActionBar(toolbar);
    }
}