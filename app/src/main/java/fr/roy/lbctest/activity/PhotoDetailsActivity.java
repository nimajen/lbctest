package fr.roy.lbctest.activity;

import com.facebook.drawee.view.SimpleDraweeView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.roy.lbctest.R;
import fr.roy.lbctest.contract.PhotoDetailsContract;
import fr.roy.lbctest.model.Photo;
import fr.roy.lbctest.presenter.PhotoDetailsPresenter;

/**
 * Display a specific photo.
 */
public class PhotoDetailsActivity extends AppCompatActivity implements PhotoDetailsContract.View {

    /**
     * Key to pass / save the Photo element
     */
    public static final String INTENT_PHOTO_KEY = "PhotoKey";

    @BindView(R.id.activity_photo_details_name)
    TextView mPhotoName;

    @BindView(R.id.activity_photo_details_picture)
    SimpleDraweeView mSimpleDraweeView;

    private PhotoDetailsContract.Presenter mPhotoDetailsPresenter;

    /**
     * Returns the intent for call this activity
     *
     * @param context the caller context
     * @param photo   the photo to display
     * @return the corresponding intent
     */
    public static Intent getIntent(Context context, Photo photo) {
        Intent intent = new Intent(context, PhotoDetailsActivity.class);
        intent.putExtra(INTENT_PHOTO_KEY, photo);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_details);
        ButterKnife.bind(this);
        initToolbar();
        initPresenter(savedInstanceState);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(INTENT_PHOTO_KEY, mPhotoDetailsPresenter.getPhoto());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void displayPhotoName(String name) {
        mPhotoName.setText(name);
    }

    @Override
    public void setPhotoUri(Uri uri) {
        mSimpleDraweeView.setImageURI(uri);
    }

    private void initPresenter(Bundle savedInstanceState) {
        mPhotoDetailsPresenter = new PhotoDetailsPresenter(this);
        mPhotoDetailsPresenter.setPhoto(loadPhotoFromBundle(savedInstanceState));
        mPhotoDetailsPresenter.start();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_photo_details_toolbar);
        setSupportActionBar(toolbar);
    }

    private Photo loadPhotoFromBundle(Bundle bundle) {
        if (bundle == null) {
            return (Photo) getIntent().getSerializableExtra(INTENT_PHOTO_KEY);
        } else {
            return (Photo) bundle.getSerializable(INTENT_PHOTO_KEY);
        }
    }

}