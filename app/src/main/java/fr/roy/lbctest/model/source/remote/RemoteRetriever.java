package fr.roy.lbctest.model.source.remote;

import java.util.ArrayList;
import java.util.List;

import fr.roy.lbctest.model.Photo;
import fr.roy.lbctest.model.PhotoRetriever;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Retrive photo list from webservice call
 */
public class RemoteRetriever implements PhotoRetriever {


    private final PhotoAPI mApi;

    /**
     * Creates a new remote retrieve
     */
    public RemoteRetriever() {
        mApi = APIManager.getInstance().getApi();
    }


    @Override
    public void loadPhotos(final LoadPhotoCallback callback, boolean forceRefresh) {
        // we don't use http cache with retrofit, ignore force refresh.
        mApi.getPhotos().enqueue(new Callback<List<PhotoJson>>() {
            @Override
            public void onResponse(Call<List<PhotoJson>> call, Response<List<PhotoJson>> response) {
                if (response != null && response.isSuccessful()) {
                    List<PhotoJson> list = response.body();
                    callback.onPhotoLoaded(convertResultToPhotoList(list));
                } else {
                    callback.onLoadError();
                }
            }

            @Override
            public void onFailure(Call<List<PhotoJson>> call, Throwable throwable) {
                callback.onLoadError();
            }
        });
    }

    private List<Photo> convertResultToPhotoList(List<PhotoJson> list) {
        ArrayList<Photo> result = new ArrayList<>(list.size());
        for (PhotoJson photoJson : list) {
            Photo photo = new Photo(photoJson.getId(), photoJson.getAlbumId(), photoJson.getTitle(), photoJson.getUrl(),
                    photoJson.getThumbnailUrl());
            result.add(photo);
        }
        return result;
    }
}
