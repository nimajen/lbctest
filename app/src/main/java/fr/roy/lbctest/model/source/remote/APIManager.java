package fr.roy.lbctest.model.source.remote;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Manage the API configuration and singleton access of the API.
 */
public class APIManager {

    private static final String BASE_URL = "http://jsonplaceholder.typicode.com";

    private static APIManager msInstance;

    private PhotoAPI mApi;

    /**
     * Get the instance of an APIManager
     *
     * @return the singleton instance
     */
    public static APIManager getInstance() {
        if (msInstance == null) {
            msInstance = new APIManager();
        }
        return msInstance;
    }

    /**
     * Private constructor
     */
    private APIManager() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();
        mApi = retrofit.create(PhotoAPI.class);
    }

    /**
     * Returns the PhotoAPI
     */
    public PhotoAPI getApi() {
        return mApi;
    }

}
