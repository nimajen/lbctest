package fr.roy.lbctest.model.source.remote;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Contains the webservice call.
 * Retrofit generates the corresponding class.
 */
public interface PhotoAPI {

    @GET("photos")
    Call<List<PhotoJson>> getPhotos();
}
