package fr.roy.lbctest.model.source.local;

import java.util.List;

import fr.roy.lbctest.model.Photo;
import fr.roy.lbctest.model.PhotoRetriever;

/**
 * Retrieve the photo list from a local source.
 */
public interface LocalPhotoRetriever extends PhotoRetriever {

    /**
     * Save the given list to the local database
     *
     * @param photoList the list to save
     */
    void savePhotoList(List<Photo> photoList);

    /**
     * Delete the list to the local database
     *
     */
    void deletePhotoList();
}
