package fr.roy.lbctest.model;

import java.io.Serializable;


/**
 * Model object for a photo. Contains the information related to a photo.
 */
public class Photo implements Serializable {

    private int mId;

    private int mAlbumId;

    private String mTitle;

    private String mUrl;

    private String mThumbnailUrl;

    public Photo(int id, int albumId, String title, String url, String thumbnailUrl) {
        mId = id;
        mAlbumId = albumId;
        mTitle = title;
        mUrl = url;
        mThumbnailUrl = thumbnailUrl;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getAlbumId() {
        return mAlbumId;
    }

    public void setAlbumId(int albumId) {
        mAlbumId = albumId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getThumbnailUrl() {
        return mThumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        mThumbnailUrl = thumbnailUrl;
    }
}
