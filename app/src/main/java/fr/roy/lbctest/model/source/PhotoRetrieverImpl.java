package fr.roy.lbctest.model.source;

import android.util.Log;

import java.util.List;

import fr.roy.lbctest.model.Photo;
import fr.roy.lbctest.model.PhotoRetriever;
import fr.roy.lbctest.model.source.local.LocalPhotoRetriever;
import fr.roy.lbctest.model.source.local.RealmPhotoRetriever;
import fr.roy.lbctest.model.source.remote.RemoteRetriever;

/**
 * Implements the logic to retrieve the photo list from different sources. (webservice, cache or database)
 */
public class PhotoRetrieverImpl implements PhotoRetriever {


    private static final String TAG = "PhotoRetrieverImpl";

    // Avoid concurrency problem: init here and not in constructor
    private static final PhotoRetrieverImpl mPhotoDataSourceImpl = new PhotoRetrieverImpl();

    private final LocalPhotoRetriever mLocalPhotoRetriever;

    private final PhotoRetriever mRemoteRetriever;

    private List<Photo> mPhotoList;

    private PhotoRetrieverImpl() {
        mLocalPhotoRetriever = initLocalPhotoRetriever();
        mRemoteRetriever = initRemotePhotoRetriever();
    }

    /**
     * Returns the instance of the photo retriever
     *
     * @return the instance of the photo retriever
     */
    public static PhotoRetrieverImpl getInstance() {
        return mPhotoDataSourceImpl;
    }


    @Override
    public void loadPhotos(final LoadPhotoCallback callback, final boolean forceRefresh) {
        if (forceRefresh) {
            mPhotoList = null;
            deletePhotoFromDatabase();
            loadPhotosFromApi(callback, true);
            return;
        }
        // check ram cache
        if (mPhotoList != null) {
            Log.i(TAG, "use cached photo list");
            callback.onPhotoLoaded(mPhotoList);
            return;
        }
        Log.d(TAG, "try to load from database...");
        // second step : load from database if it's available
        loadPhotoFromDatabase(new LoadPhotoCallback() {

            @Override
            public void onPhotoLoaded(List<Photo> photoList) {
                Log.i(TAG, "use photo list from database. Size : " + photoList.size());
                onPhotoLoadedFinish(callback, photoList);
            }

            @Override
            public void onLoadError() {
                // not available from database : load photo from API
                loadPhotosFromApi(callback, false);
            }
        });

    }

    // override for mock
    protected LocalPhotoRetriever initLocalPhotoRetriever() {
        return new RealmPhotoRetriever();
    }

    // override for mock
    protected RemoteRetriever initRemotePhotoRetriever() {
        return new RemoteRetriever();
    }

    private void onPhotoLoadedFinish(LoadPhotoCallback callback, List<Photo> photoList) {
        mPhotoList = photoList;
        callback.onPhotoLoaded(photoList);
    }

    private void loadPhotosFromApi(final LoadPhotoCallback callback, boolean forceRefresh) {
        Log.i(TAG, "loading photo from api...");
        mRemoteRetriever.loadPhotos(new LoadPhotoCallback() {
            @Override
            public void onPhotoLoaded(List<Photo> photoList) {
                Log.i(TAG, "Use photo list from API. Size : " + photoList.size());
                onPhotoLoadedFinish(callback, photoList);
                savePhotoToDatabase(photoList);
            }

            @Override
            public void onLoadError() {
                Log.e(TAG, "Error from api");
                callback.onLoadError();
            }
        }, forceRefresh);
    }

    private void savePhotoToDatabase(List<Photo> photoList) {
        mLocalPhotoRetriever.savePhotoList(photoList);
    }

    private void deletePhotoFromDatabase() {
        mLocalPhotoRetriever.deletePhotoList();
    }

    private void loadPhotoFromDatabase(LoadPhotoCallback callback) {
        // force refresh is always false if we load data from database.
        mLocalPhotoRetriever.loadPhotos(callback, false);
    }

}
