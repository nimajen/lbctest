package fr.roy.lbctest.model;

import java.util.List;

/**
 * Access point for retrieve the photo list from different sources.
 */
public interface PhotoRetriever {

    /**
     * Load the photo list asynchronously. The LoadPhotoCallback.onPhotoLoaded method will be called if the photo list is correctly loaded.
     * Otherwise, the onLoadError method is called.
     *
     * @param callback     the corresponding callback
     * @param forceRefresh if true, don't use the cached data and load it from the network directly
     */
    void loadPhotos(LoadPhotoCallback callback, boolean forceRefresh);

    /**
     * Callback linked with the PhotoRetriever.loadPhotos method.
     */
    interface LoadPhotoCallback {

        /**
         * Called if the list is successfully loaded
         *
         * @param photoList the corresponding list
         */
        void onPhotoLoaded(List<Photo> photoList);

        /**
         * Called if an error occurs
         */
        void onLoadError();
    }
}
