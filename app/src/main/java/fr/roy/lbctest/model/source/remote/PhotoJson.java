package fr.roy.lbctest.model.source.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Json object for a photo.
 */
public class PhotoJson implements Serializable {

    @SerializedName("id")
    @Expose
    private int mId;

    @SerializedName("albumId")
    @Expose
    private int mAlbumId;

    @SerializedName("title")
    @Expose
    private String mTitle;

    @SerializedName("url")
    @Expose
    private String mUrl;

    @SerializedName("thumbnailUrl")
    @Expose
    private String mThumbnailUrl;

    public PhotoJson() {
        // empty for gson constructor
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getAlbumId() {
        return mAlbumId;
    }

    public void setAlbumId(int albumId) {
        mAlbumId = albumId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getThumbnailUrl() {
        return mThumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        mThumbnailUrl = thumbnailUrl;
    }
}
