package fr.roy.lbctest.model.source.local;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import fr.roy.lbctest.model.Photo;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Retrieve photo list from local database
 */
public class RealmPhotoRetriever implements LocalPhotoRetriever {

    @Override
    public void loadPhotos(LoadPhotoCallback callback, boolean forceRefresh) {
        LoadPhotoTask task = new LoadPhotoTask(callback);
        task.execute();
    }

    @Override
    public void savePhotoList(List<Photo> photoList) {
        SavePhotoTask task = new SavePhotoTask(photoList);
        task.execute();
    }

    @Override
    public void deletePhotoList() {
        DeletePhotoTask task = new DeletePhotoTask();
        task.execute();
    }

    /**
     * Load the photo list from database asynchronously
     */
    private static class LoadPhotoTask extends AsyncTask<Void, Void, List<Photo>> {

        private final LoadPhotoCallback mLoadPhotoCallback;

        private LoadPhotoTask(LoadPhotoCallback callback) {
            mLoadPhotoCallback = callback;

        }

        @Override
        protected List<Photo> doInBackground(Void... voids) {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<PhotoDatabaseObject> result = realm.where(PhotoDatabaseObject.class).findAllSorted("mId", Sort.ASCENDING);
            // now convert it to photo
            List<Photo> list = convertDatabaseListToPhotoList(result);
            realm.close();
            return list;
        }

        @Override
        protected void onPostExecute(List<Photo> list) {
            // also call onLoadError is the list is empty : it means we have no data in cache
            if (list == null || list.isEmpty()) {
                mLoadPhotoCallback.onLoadError();
            } else {
                mLoadPhotoCallback.onPhotoLoaded(list);
            }
        }

        private List<Photo> convertDatabaseListToPhotoList(RealmResults<PhotoDatabaseObject> result) {
            if (result.isEmpty()) {
                return new ArrayList<>();
            }
            List<Photo> photoList = new ArrayList<>(result.size());
            for (PhotoDatabaseObject photoDatabaseObject : result) {
                Photo photo = new Photo(photoDatabaseObject.getId(), photoDatabaseObject.getAlbumId(), photoDatabaseObject.getTitle(),
                        photoDatabaseObject.getUrl(), photoDatabaseObject.getThumbnailUrl());
                photoList.add(photo);
            }
            return photoList;
        }
    }

    /**
     * Save the photo list to the local database asynchronously
     */
    private class SavePhotoTask extends AsyncTask<Void, Void, Void> {

        private final List<Photo> mPhotoList;


        private SavePhotoTask(List<Photo> photoList) {
            mPhotoList = photoList;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(PhotoDatabaseObject.class);
                    for (Photo photo : mPhotoList) {
                        PhotoDatabaseObject photoDatabaseObject = realm.createObject(PhotoDatabaseObject.class);
                        photoDatabaseObject.setId(photo.getId());
                        photoDatabaseObject.setAlbumId(photo.getAlbumId());
                        photoDatabaseObject.setTitle(photo.getTitle());
                        photoDatabaseObject.setThumbnailUrl(photo.getThumbnailUrl());
                        photoDatabaseObject.setUrl(photo.getUrl());
                    }
                }
            });
            realm.close();
            return null;
        }
    }

    /**
     * Delete the photo list to the local database asynchronously
     */
    private class DeletePhotoTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(PhotoDatabaseObject.class);
                }
            });
            realm.close();
            return null;
        }
    }
}
